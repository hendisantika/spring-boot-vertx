package com.hendisantika.springbootvertx.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.net.NetServer;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-vertx
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/07/18
 * Time: 19.16
 * To change this template use File | Settings | File Templates.
 */
public class PushTcpServer extends AbstractVerticle {

    private NetServer server;
    private EventBus eventBus;

    public EventBus getEventBus() {
        return vertx.eventBus();
    }

    @Override
    public void start() {
        server = vertx.createNetServer();
        eventBus = vertx.eventBus();

        server.connectHandler(socket ->
                eventBus.consumer("tcp.push.message", message -> {
                    String body = (String) message.body();
                    socket.write(body);
                    message.reply(body);
                })
        );
        server.listen(8888);
    }
}